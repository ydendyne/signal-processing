import sys
import numpy as peen
import math
import statistics
import Preprocessing.NoiseCancellation
from PIL import Image


def main(image_matrix):
    # add rows and columns assuming a 3x3 kernel
    r, c = image_matrix.shape
    im_shape = (r + 2, c + 2)
    convolution_im = peen.zeros(im_shape, dtype=int)

    # copying original image into a matrix for convolution
    for i in range(r):
        for j in range(c):
            convolution_im[i + 1][j + 1] = image_matrix[i][j]

    # noise cancellation filter kernel
    noise_filter_kernel = peen.array([[1 / 9, 1 / 9, 1 / 9], [1 / 9, 1 / 9, 1 / 9], [1 / 9, 1 / 9, 1 / 9]])
    mean_filter = Preprocessing.NoiseCancellation.convolution(convolution_im, noise_filter_kernel)
    # mean_filter = image_matrix
    threshold = 160
    filtered_image = peen.zeros(im_shape)
    for i in range(r):
        for j in range(c):
            if mean_filter[i][j] < threshold:
                filtered_image[i][j] = 0
            else:
                filtered_image[i][j] = 255
            # filtered_image[i][j] = (mean_filter[i][j] < threshold)?0:255

    img = Image.fromarray(filtered_image)
    img.show()


if __name__ == '__main__':
    im = Image.open(sys.argv[1])
    im_array = peen.array(im)
    main(im_array)

