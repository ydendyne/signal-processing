import sys
import numpy as peen
import math
import statistics
from PIL import Image


def median_convolution(im_c_array):
    # r - rows, c - columns
    r, c = im_c_array.shape
    kernel_size = 3
    f_image_shape = (r - 2, c - 2)
    convolved_image = peen.zeros(f_image_shape, dtype=int)

    # to find the middle of the kernel
    offset = int(kernel_size / 2)

    # iterating over image
    for i in range(1, r - 1):
        for j in range(1, c - 1):
            pixel_value = []

            # iterative over kernel
            for i_k in range(kernel_size):
                for j_k in range(kernel_size):
                    pixel_value.append(im_c_array[i - offset + i_k][j - offset + j_k])

            convolved_image[i - 1][j - 1] = statistics.median(pixel_value)

    return convolved_image


def convolution(im_c_array, kernel):
    # r - rows, c - columns
    r, c = im_c_array.shape
    r_k, c_k = kernel.shape
    f_image_shape = (r - 2, c - 2)
    convolved_image = peen.zeros(f_image_shape, dtype=int)

    # to find the middle of the kernel
    offset = int(r_k / 2)

    # iterating over image
    for i in range(1, r - 1):
        for j in range(1, c - 1):
            pixel_value = 0

            # iterative over kernel
            for i_k in range(r_k):
                for j_k in range(c_k):
                    pixel_value += kernel[i_k][j_k] * im_c_array[i - offset + i_k][j - offset + j_k]

            convolved_image[i - 1][j - 1] = pixel_value

    return convolved_image


def main(image_matrix, mode):
    # add rows and columns assuming a 3x3 kernel
    r, c = image_matrix.shape
    im_shape = (r + 2, c + 2)
    convolution_im = peen.zeros(im_shape, dtype=int)

    # copying original image into a matrix for convolution
    for i in range(r):
        for j in range(c):
            convolution_im[i + 1][j + 1] = image_matrix[i][j]

    # noise cancellation filter kernel
    if mode.lower() == "mean":
        noise_filter_kernel = peen.array([[1/9, 1/9, 1/9], [1/9, 1/9, 1/9], [1/9, 1/9, 1/9]])
        mean_filter = convolution(convolution_im, noise_filter_kernel)

        filtered_image = peen.zeros(im_shape)
        for i in range(r):
            for j in range(c):
                filtered_image[i][j] = mean_filter[i][j]

        img = Image.fromarray(filtered_image)

        img.show()

    elif mode.lower() == "median":
        median_filter = median_convolution(convolution_im)

        filtered_image = peen.zeros(im_shape)
        for i in range(r):
            for j in range(c):
                filtered_image[i][j] = median_filter[i][j]

        img = Image.fromarray(filtered_image)
        img.show()
    else:
        print("Invalid mode argument: needs to be mean or median")


if __name__ == '__main__':
    im = Image.open(sys.argv[1])
    im_array = peen.array(im)
    main(im_array, sys.argv[2])

