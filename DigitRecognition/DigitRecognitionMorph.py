import os
import sys
import numpy as np
from sklearn.tree import DecisionTreeClassifier
# from sklearn.externals.six import StringIO
from sklearn.tree import export_graphviz
from pydot import graph_from_dot_data
from IPython.display import Image
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix


def main(i_dir):
    mor = []

    combined_data = []

    # load data from each file
    for file in os.listdir(i_dir):
        fp = open(i_dir + "/" + file)
        if file == "mfeat-mor":
            for line in fp:
                mor.append(line.split())

    # combine data sets
    for i in range(0, 2000):
        combined_data.append([])
        combined_data[i] = combined_data[i] + mor[i]

    # add class to each instance
    for i in range(0, 200):
        combined_data[i].append(0)
    for i in range(200, 400):
        combined_data[i].append(1)
    for i in range(400, 600):
        combined_data[i].append(2)
    for i in range(600, 800):
        combined_data[i].append(3)
    for i in range(800, 1000):
        combined_data[i].append(4)
    for i in range(1000, 1200):
        combined_data[i].append(5)
    for i in range(1200, 1400):
        combined_data[i].append(6)
    for i in range(1400, 1600):
        combined_data[i].append(7)
    for i in range(1600, 1800):
        combined_data[i].append(8)
    for i in range(1800, 2000):
        combined_data[i].append(9)

    # split into train and test
    train_instance = combined_data[0:100] + combined_data[200:300] + combined_data[400:500] + combined_data[600:700] + combined_data[800:900] + combined_data[1000:1100] + combined_data[1200:1300] + combined_data[1400:1500] + combined_data[1600:1700] + combined_data[1800:1900]
    test_instance = combined_data[100:200] + combined_data[300:400] + combined_data[500:600] + combined_data[700:800] + combined_data[900:1000] + combined_data[1100:1200] + combined_data[1300:1400] + combined_data[1500:1600] + combined_data[1700:1800] + combined_data[1900:2000]

    train_instance = np.array(train_instance)
    test_instance = np.array(test_instance)

    # remove instance label and put it in a seperate list
    train_class = train_instance[:, -1]
    train_instance = train_instance[:, :-1]

    test_class = test_instance[:, -1]
    test_instance = test_instance[:, :-1]

    # decision tree classifier
    model = DecisionTreeClassifier()
    fitted_model = model.fit(train_instance, train_class)
    y_train_predict = fitted_model.predict(train_instance)
    y_pred = fitted_model.predict(test_instance)

    print(confusion_matrix(test_class, y_pred))

    # results
    accuracy = accuracy_score(test_class, y_pred)
    print("Accuracy: %f" % (accuracy))

    return 0


if __name__ == '__main__':
    main(sys.argv[1])
