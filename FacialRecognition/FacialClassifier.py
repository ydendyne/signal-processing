import sys
from sklearn.naive_bayes import GaussianNB
import numpy as np
from sklearn.metrics import plot_roc_curve
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score


def main(train_vec_file, test_vec_file):
    train_vectors = []
    train_vectors_class = []

    train_file = open(train_vec_file, "r")
    for line in train_file:
        vec = line.split(" ")
        train_vectors_class.append(vec[-2])
        train_vectors.append(vec[0:-2])
    train_file.close()
    # print(train_vectors)
    train_vectors = np.array(train_vectors).astype(np.float64)
    train_vectors_class = np.array(train_vectors_class).astype(np.float64)

    test_vectors = []
    test_vectors_class = []

    test_file = open(train_vec_file, "r")
    for line in test_file:
        vec = line.split(" ")
        test_vectors_class.append(vec[-2])
        test_vectors.append(vec[0:-2])

    test_vectors = np.array(test_vectors).astype(np.float64)
    test_vectors_class = np.array(test_vectors_class).astype(np.float64)

    # your_array.astype(np.float64)
    # train_vectors = train_vectors.astype(np.float64)
    # --------------------------------------

    model = GaussianNB()
    fitted_model = model.fit(train_vectors, train_vectors_class)

    # y_train_predict = fitted_model.predict(train_vectors)
    y_pred = fitted_model.predict(test_vectors)

    accuracy = accuracy_score(test_vectors_class, y_pred)
    print("Accuracy: %f" % (accuracy))

    roc_plot = plot_roc_curve(model, test_vectors, test_vectors_class)

    plt.show()

    return 0


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
