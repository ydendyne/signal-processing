import sys
import os
import math
from PIL import Image
import numpy as peen
from Preprocessing import EdgeDetection
import matplotlib.pyplot as plt


# returns the index of the highest populated bin for the local angles
def local_angle_feature(start_x, end_x, start_y, end_y, dx_array, dy_array):
    angles = [0] * 30
    for i in range(start_x, end_x+1):
        for j in range(start_y, end_y+1):
            pi_on_15 = math.pi / 15
            ang = math.atan2(dy_array[i][j], dx_array[i][j])

            # make sure there is an edge
            if min(math.sqrt(pow(dx_array[i][j], 2) + pow(dy_array[i][j], 2)), 255) > 100:
                if -math.pi <= ang < (-math.pi) + pi_on_15:
                    angles[0] += 1
                elif (-math.pi) + pi_on_15 <= ang < (-math.pi) + pi_on_15 * 2:
                    angles[1] += 1
                elif (-math.pi) + pi_on_15 * 2 <= ang < (-math.pi) + pi_on_15 * 3:
                    angles[2] += 1
                elif (-math.pi) + pi_on_15 * 3 <= ang < (-math.pi) + pi_on_15 * 4:
                    angles[3] += 1
                elif (-math.pi) + pi_on_15 * 4 <= ang < (-math.pi) + pi_on_15 * 5:
                    angles[4] += 1
                elif (-math.pi) + pi_on_15 * 5 <= ang < (-math.pi) + pi_on_15 * 6:
                    angles[5] += 1
                elif (-math.pi) + pi_on_15 * 6 <= ang < (-math.pi) + pi_on_15 * 7:
                    angles[6] += 1
                elif (-math.pi) + pi_on_15 * 7 <= ang < (-math.pi) + pi_on_15 * 8:
                    angles[7] += 1
                elif (-math.pi) + pi_on_15 * 8 <= ang < (-math.pi) + pi_on_15 * 9:
                    angles[8] += 1
                elif (-math.pi) + pi_on_15 * 9 <= ang < (-math.pi) + pi_on_15 * 10:
                    angles[9] += 1
                elif (-math.pi) + pi_on_15 * 10 <= ang < (-math.pi) + pi_on_15 * 11:
                    angles[10] += 1
                elif (-math.pi) + pi_on_15 * 11 <= ang < (-math.pi) + pi_on_15 * 12:
                    angles[11] += 1
                elif (-math.pi) + pi_on_15 * 12 <= ang < (-math.pi) + pi_on_15 * 13:
                    angles[12] += 1
                elif (-math.pi) + pi_on_15 * 13 <= ang < (-math.pi) + pi_on_15 * 14:
                    angles[13] += 1
                elif (-math.pi) + pi_on_15 * 14 <= ang < (-math.pi) + pi_on_15 * 15:
                    angles[14] += 1
                elif 0 <= ang < pi_on_15:
                    angles[15] += 1
                elif pi_on_15 <= ang < pi_on_15 * 2:
                    angles[16] += 1
                elif pi_on_15 * 2 <= ang < pi_on_15 * 3:
                    angles[17] += 1
                elif pi_on_15 * 3 <= ang < pi_on_15 * 4:
                    angles[18] += 1
                elif pi_on_15 * 4 <= ang < pi_on_15 * 5:
                    angles[19] += 1
                elif pi_on_15 * 5 <= ang < pi_on_15 * 6:
                    angles[20] += 1
                elif pi_on_15 * 6 <= ang < pi_on_15 * 7:
                    angles[21] += 1
                elif pi_on_15 * 7 <= ang < pi_on_15 * 8:
                    angles[22] += 1
                elif pi_on_15 * 8 <= ang < pi_on_15 * 9:
                    angles[23] += 1
                elif pi_on_15 * 9 <= ang < pi_on_15 * 10:
                    angles[24] += 1
                elif pi_on_15 * 10 <= ang < pi_on_15 * 11:
                    angles[25] += 1
                elif pi_on_15 * 11 <= ang < pi_on_15 * 12:
                    angles[26] += 1
                elif pi_on_15 * 12 <= ang < pi_on_15 * 13:
                    angles[27] += 1
                elif pi_on_15 * 13 <= ang < pi_on_15 * 14:
                    angles[28] += 1
                elif pi_on_15 * 14 <= ang < pi_on_15 * 15:
                    angles[29] += 1
            # else:
            #     print("fuck")
            # angles.append(math.atan2(dy_array[i][j], dx_array[i][j]))

    index_of_max_bin = 0
    max = 0
    for i in range(len(angles)):
        if angles[i] > max:
            max = angles[i]
            index_of_max_bin = i

    # print("asdf " + str(min(angles)) + " " + str(max(angles)))
    # plt.plot(angles)
    # plt.show()
    return index_of_max_bin


def local_number(x, y, image_array):
    num = image_array[x][y]
    sum_binary_number = 0
    if image_array[x - 1][y - 1] >= num:
        sum_binary_number += 128
    if image_array[x][y - 1] >= num:
        sum_binary_number += 64
    if image_array[x + 1][y - 1] >= num:
        sum_binary_number += 32
    if image_array[x + 1][y] >= num:
        sum_binary_number += 16
    if image_array[x + 1][y + 1] >= num:
        sum_binary_number += 8
    if image_array[x][y + 1] >= num:
        sum_binary_number += 4
    if image_array[x - 1][y + 1] >= num:
        sum_binary_number += 2
    if image_array[x - 1][y] >= num:
        sum_binary_number += 1

    return sum_binary_number


# returns the index of the highest populated bin for the local feature
def local_binary_feature(start_x, end_x, start_y, end_y, image_array):
    bins = [0]*30
    for i in range(start_x, end_x+1):
        for j in range(start_y, end_y+1):
            x = local_number(i, j, image_array)
            for k in range(len(bins)):
                if (255/30) * k <= x < (255/30) * (k + 1):
                    bins[i] += 1

    max_bin = 0
    max_index = 0
    for i in range(len(bins)):
        if bins[i] > max_bin:
            max_bin = bins[i]
            max_index = i

    return max_index


def main(i_dir, o_file):
    fp = open(o_file, "w")

    # feature vectors
    vectors = []

    for file in os.listdir(i_dir):
        print(file)

        for image in os.listdir(input_dir + "/" + file):
            # current vector
            c_vector = []

            im = Image.open(i_dir + "/" + file + "/" + image)
            im_array = peen.array(im)

            r, c = im_array.shape
            im_shape = (r + 2, c + 2)
            convolution_im = peen.zeros(im_shape, dtype=int)

            # copying original image into a matrix for convolution
            for i in range(r):
                for j in range(c):
                    convolution_im[i + 1][j + 1] = im_array[i][j]

            # derivative in x and y kernels
            df_dx_kernel = peen.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
            df_dy_kernel = peen.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]])

            # applying the derivatives to the image
            dx = EdgeDetection.convolution(convolution_im, df_dx_kernel)
            dy = EdgeDetection.convolution(convolution_im, df_dy_kernel)

            # global angle feature
            c_vector.append(local_angle_feature(0, 18, 0, 18, dx, dy))
            c_vector.append(peen.mean(im_array))
            # average brightness

            # left eye LBF, LAF
            c_vector.append(local_angle_feature(1, 8, 1, 6, dx, dy))
            c_vector.append(local_binary_feature(1, 8, 1, 6, im_array))

            # right eye LBF, LAF
            c_vector.append(local_angle_feature(10, 17, 1, 6, dx, dy))
            c_vector.append(local_binary_feature(10, 17, 1, 6, im_array))
            # nose LBF, LAF
            c_vector.append(local_angle_feature(6, 13, 4, 13, dx, dy))
            c_vector.append(local_binary_feature(6, 13, 4, 13, im_array))
            # mouth LBF, LAF
            c_vector.append(local_angle_feature(3, 15, 13, 17, dx, dy))
            c_vector.append(local_binary_feature(3, 15, 13, 17, im_array))

            if file == "face":
                c_vector.append(1)
            elif file == "non-face":
                c_vector.append(0)
            else:
                print("Incorrect Directory, requires face and non face sub directories")
                return
            vectors.append(c_vector)

        # print(im_array.shape)
        # i -=- 1
    # print(str(i) + ", " + str(j))
    for vec in vectors:
        for i in vec:
            fp.write(str(i)+" ")
        fp.write("\n")
    fp.close()
    # print(vectors)


if __name__ == '__main__':
    input_dir = sys.argv[1]
    output_file = sys.argv[2]
    main(input_dir, output_file)
